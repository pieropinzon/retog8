#!/usr/bin/env python

import mandrill
import datetime

def  send_mail(subject, content, send_to, api_key, html=False):
	log = {}
	log['start'] = datetime.datetime.now().isoformat()
	log['text']  = 'Start: ' + log['start'] + '\n'
	log['text']  = 'Mail Subjetc: ' + subject + '\n'
	try:
		mandrill_client = mandrill.Mandrill(api_key)
		message = {
			'from_email': 'g8logs@gmail.com',
			'from_name': 'Mayordomo G8',
			'important': False,
			'metadata': {'website': 'corporaciong8.com'},
			'subject': subject,
			'to': [{'email': send_to,
             		'name': 'corporaciong8.com',
             		'type': 'to'}]
		}
		if html:
			message['html'] = content
		else:
			message['text'] = content
		results = mandrill_client.messages.send(message=message)
		for result in results:
			log['text'] += "Email: " + result['email'] + "\nStatus: " + \
				result['status'] + '\nId: ' + result['_id'] + \
				'\nReject Reason: ' + str(result['reject_reason']) + '\n'
	except mandrill.Error, e:
	 	log['text'] += 'Error %s - %s\n' % (e.__class__, e)
	finally:
	 	log['end'] = datetime.datetime.now().isoformat()
	 	log['text'] += 'End: ' + log['end']
	print log

if __name__ == '__main__':
	send_mail()