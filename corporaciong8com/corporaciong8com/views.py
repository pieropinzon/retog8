from pyramid.view import view_config, notfound_view_config

from deform import Form, ValidationFailure
from .schemas.forms import FormContacto

from .mail import send_mail

@view_config(route_name='home', renderer='templates/index.pt')
def my_view(context, request):
	form = Form(FormContacto() , buttons=('submit',))

	return {'form': form.render(),
            'url': 'enviado'}

@notfound_view_config(renderer='templates/notfound.pt')
def notfound(request):
    request.response.status = 404
    return {}

@view_config(route_name='enviado', renderer='templates/enviado.pt')
def envio_form(context, request):

	if 'submit' in request.params:

		nombre  = request.POST.get('name')
		email   = request.POST.get('email')
		tlf     = request.POST.get('phone')
		mensaje = request.POST.get('message')
		api_key = request.registry.settings['mandrill-api-key']
		msg     = 'Nombre y Apellido: ' + nombre.title() +'\n'
		msg    += 'Correo Electronico: ' + email + '\n'
		msg    += 'Numero Telefonico: ' + tlf + '\n'
		msg    += 'Mensaje: ' + mensaje + '\n'

		send_mail('Mensaje desde CorporacionG8.com', msg , 'etalamo@corporaciong8.com', api_key)

	return {'enviado': 'exitoso'}