# -*- coding: utf-8 -*-

import colander

class FormContacto(colander.MappingSchema):

    allow_extra_fields = True
    filter_extra_fields = True

    nombre    = colander.SchemaNode(colander.String(colander.Length(max=20)))
    email     = colander.SchemaNode(colander.String(colander.Email()))
    tlf       = colander.SchemaNode(colander.Integer())
    mensaje   = colander.SchemaNode(colander.String(colander.Length(max=1000)))